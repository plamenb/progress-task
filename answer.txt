Hello John,

Thanks for getting in touch, I’m sorry we weren’t able to meet your expectations last time but I'd be more than happy to send an
example of the application working correctly on both browser and mobile, meeting the requirements specified. 

Please find attached an example. Let me know what you think and if this suits your needs.

Thank you for choosing us and I look forward to hearing again from you soon!

Regards,
Plamen
