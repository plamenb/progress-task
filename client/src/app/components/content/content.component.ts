import { Component, OnInit } from '@angular/core';
import { DatasService } from 'src/app/services/data.service';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit {

  public projectData = [];

  constructor(private readonly data: DatasService) { }

  ngOnInit() {
    this.projectData = this.data.dataService().projects;
  }

  public chooseAvatar() {
    return `assets/img/avatar${Math.ceil(Math.random() * 4)}.png`;
  }

}
