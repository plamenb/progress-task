# PROGRESS TASK

## Description
A single page application that displays data about users in projects.
---
## Technologies used:
- [Angular](https://angular.io/)
- HTML
- CSS
---
## Installation

Clone the repository:

```git clone https://gitlab.com/plamenb/progress-task.git```

Install dependencies in the ```client``` folder:

```npm install```

---
## Development server

Run ```ng serve -o``` which will start a dev server and navigate you to ```http://localhost:4200/```.

---
## Author
- [Plamen Bradvarev](https://gitlab.com/plamenb)